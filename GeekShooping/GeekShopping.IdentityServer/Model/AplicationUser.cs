﻿using Microsoft.AspNetCore.Identity;

namespace GeekShopping.IdentityServer.Model
{
    public class AplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
